<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'projects'
], function($router){
    Route::get('/', 'ProjectsController@getAll');
    Route::get('/{id}', 'ProjectsController@get');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'tasks'
], function($router){

    Route::get('/', 'TasksController@getAll');
    Route::get('/{id}', 'TasksController@get');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'users'
], function($router){

    Route::get('/', 'UsersController@getAll');
    Route::get('/{id}', 'UsersController@get');
});