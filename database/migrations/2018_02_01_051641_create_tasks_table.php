<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_task');
            $table->string('name');
            $table->text('description');
            $table->string('alias')->unique();
            $table->boolean('status');
            $table->dateTime('initial_date');
            $table->dateTime('final_date');
            $table->float('complete')->default(0.0);
            $table->json('duration')->nullable();
            $table->integer('project_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('tasks');
    }
}
