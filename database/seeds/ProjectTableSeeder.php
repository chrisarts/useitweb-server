<?php

use App\Project;
use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $project = new Project();
        $project->name = 'Entrevista Chris';
        $project->description = 'Servidor Rest Laravel y App ReactNative - Gestión de Proyectos';
        $project->avatar = 'standard';
        $project->alias = 'PhantomInterview';
        $project->status = true;
        $project->initial_date = new DateTime('2018-01-31');
        $project->final_date = new DateTime('2018-02-02');
        $project->user_id = 2;
        $project->save();
    }
}
