<?php

use App\Task;
use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $task = new Task();
        $task->parent_task = 0;
        $task->name = 'Crear Servidor Para PhantonInterview';
        $task->description = 'Iniciar un nuevo proyecto en Laravel para el proyecto PhantomInterview';
        $task->alias = '[PhantomInterview] - Crear Servidor';
        $task->status = true;
        $task->initial_date = new DateTime();
        $task->final_date = new DateTime();
        $task->complete = 10.5;
        $task->project_id = 1;
        $task->user_id = 2;
        $task->save();
    }
}
