<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        $admin = new User();
        $admin->name = 'Some Admin';
        $admin->avatar = 'standard';
        $admin->email = 'admin@localhost.com';
        $admin->password = bcrypt('secret');
        $admin->role = 'admin';
        $admin->save();

        $leader = new User();
        $leader->name = 'Some Leader';
        $leader->avatar = 'standard';
        $leader->email = 'leader@localhost.com';
        $leader->password = bcrypt('secret');
        $leader->role = 'leader';
        $leader->save();

        $member = new User();
        $member->name = 'Some Member';
        $member->avatar = 'standard';
        $member->email = 'member@localhost.com';
        $member->password = bcrypt('secret');
        $member->role = 'member';
        $member->save();
    }
}
