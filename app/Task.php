<?php

namespace App;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property int parent_task
 * @property string description
 * @property string alias
 * @property string status
 * @property DateTime initial_date
 * @property DateTime final_date
 * @property float complete
 * @property DateTime duration
 * @property int project_id
 * @property int user_id
 */
class Task extends Model{
    protected $casts = [
        'duration' => 'array'
    ];

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function owner(){
        return $this->hasOne(User::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function comments(){
        return $this->hasMany(TaskComment::class);
    }
}
