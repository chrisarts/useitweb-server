<?php

namespace App;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string description
 * @property string avatar
 * @property string alias
 * @property bool status
 * @property DateTime initial_date
 * @property DateTime final_date
 * @property int leader_user
 * @property int leader_user_id
 * @property int user_id
 */
class Project extends Model{
    public function leader(){
        return $this->belongsTo(User::class);
    }

    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
