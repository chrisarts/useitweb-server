<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectsController extends Controller{

    function __construct(){
        $this->middleware('auth');
    }

    public function getAll(){
        //$request->user()->authorizeRoles(['Administrador', 'Lider', 'Registrado']);
        $projects = Project::all();
        return response()->json($projects->toArray());
    }

    public function get($id){
        $project = Project::find($id);
        return response()->json($project);
    }
}