<?php
/**
 * User: chris
 * Date: 3/02/18
 * Time: 03:33 PM
 */

namespace App\Http\Controllers;

use App\User;

class UsersController extends Controller{

    function __construct(){
        $this->middleware('auth');
    }

    public function getAll(){
        //$request->user()->authorizeRoles(['Administrador', 'Lider', 'Registrado']);
        $users = User::all();
        return response()->json($users->toArray());
    }

    public function get($id){
        $user = User::find($id);
        return response()->json($user);
    }
}