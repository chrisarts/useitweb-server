<?php
/**
 * User: chris
 * Date: 3/02/18
 * Time: 03:22 PM
 */
namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Task;

class TasksController extends Controller{

    function __construct(){
        $this->middleware('auth');
    }

    public function getAll(){
        //$request->user()->authorizeRoles(['Administrador', 'Lider', 'Registrado']);
        $tasks = Task::all();
        return response()->json($tasks->toArray());
    }

    public function get($id){
        $task = Task::find($id);
        return response()->json($task);
    }
}