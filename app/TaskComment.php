<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model{
    protected $casts = [
        'tags' => 'array'
    ];

    public function task(){
        return $this->belongsToMany(Task::class);
    }
}
